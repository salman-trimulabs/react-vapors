import gql from "graphql-tag";

const ALL_USERS_QUERY = gql`
  query {
    users {
      id
      email
      name
      organization {
        id
        title
      }
    }
  }
`;

const ME_QUERY = gql`
  query {
    me {
      name
      organization {
        title
      }
    }
  }
`;

export default {
  ALL_USERS_QUERY,
  ME_QUERY,
};
