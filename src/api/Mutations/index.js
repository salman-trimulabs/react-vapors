import gql from "graphql-tag";

const LOGIN = gql`
  mutation userLogin($email: String!, $password: String!) {
    userLogin(email: $email, password: $password) {
      credentials {
        uid
        client
        expiry
        accessToken
      }
      authenticatable {
        name
      }
    }
  }
`;
const LOGOUT = gql`
  mutation {
    userLogout {
      authenticatable {
        name
      }
    }
  }
`;

export default {
  LOGIN,
  LOGOUT,
};
