import URLS from "../../constants";
import ApolloClient from "apollo-boost";
import { InMemoryCache } from "apollo-cache-inmemory";
import { getSession } from "../../helpers/SessionManagement";

const cache = new InMemoryCache();

class ApolloClientObj {
  constructor() {
    this.client = new ApolloClient({
      request: operation => {
        const session = getSession();
        operation.setContext({
          headers: {
            uid: session.uid,
            "token-type": "Bearer",
            "access-token": session.accessToken,
            client: session.client
          }
        });
      },
      uri: URLS.API_BASE_URL + 'graphql',
      cache
    });
  }
}


class ApolloAuthClientObj {
  constructor() {
    this.client = new ApolloClient({
      request: operation => {
        const session = getSession();
        if (session) {
          operation.setContext({
            headers: {
              uid: session.uid,
              "token-type": "Bearer",
              "access-token": session.accessToken,
              client: session.client
            }
          });
        }
      },
      uri: URLS.API_BASE_URL + 'auth',
      cache
    });
  }
}

const ApolloAuthClient = new ApolloAuthClientObj();
const ApolloDataClient = new ApolloClientObj();


const clearClientCache = () => {
  ApolloDataClient.client.cache.reset();
  ApolloAuthClient.client.cache.reset();
};

export default ApolloDataClient;
export { clearClientCache, ApolloAuthClient };
