import Landing from '../screens/dashboard';
import Profile from '../screens/profile';
import SignUp from '../screens/signup';
const Routes = [
  {
    path: '/',
    name: 'landing',
    exact: true,
    component: Landing,
  },
  {
    path: '/profile',
    name: 'profile',
    exact: true,
    component: Profile,
  },
  {
    path: '/signup',
    name: 'signup',
    exact: true,
    component: SignUp,
  }
];

export default Routes;
