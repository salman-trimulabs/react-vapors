const USER_SESSION_KEY = "USER_SESSION_KEY_V1";

function saveSession(credentials) {
  localStorage.setItem(USER_SESSION_KEY, JSON.stringify(credentials));
  return true;
}

function getSession() {
  let session = localStorage.getItem(USER_SESSION_KEY);
  return session ? JSON.parse(session) : undefined
}

function deleteSession() {
  localStorage.removeItem(USER_SESSION_KEY);
  return true;
}

function isSession() {
  let session = localStorage.getItem(USER_SESSION_KEY);
  return !!session;
}

export {
  saveSession,
  getSession,
  deleteSession,
  isSession
};
