import React from "react";
import { Input, Button } from "antd";
import styled from "styled-components";
import { ThemeColors } from "../../theme";
import { ApolloAuthClient } from "../../api";
import { ApolloProvider } from "react-apollo";

const Wrapper = styled.div`
  background: ${ThemeColors.colors.default};
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Form = styled.form`
  max-width: 600px;
  width: 100%;
  padding: 1rem;
  height: 250px;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
`;

export function SignUp(props) {
  const { setAuth } = props;
  return (
    <Wrapper>
      <Form>
        <div>SignUp Here</div>
        <Input
          size={"large"}
          placeholder="Your Full Name"
          onChange={(e) => console.log(e.target.value)}
        />
        <Input
          size={"large"}
          placeholder="Your email address"
          onChange={(e) => console.log(e.target.value)}
        />
        <Input.Password
          size={"large"}
          placeholder="Your password"
          onChange={(e) => console.log(e.target.value)}
        />
        <Button type="primary" size={"large"} onClick={() => setAuth(true)}>
          SignUp
        </Button>
      </Form>
    </Wrapper>
  );
}

export default function SignUpProvider(props) {
  return (
    <ApolloProvider client={ApolloAuthClient.client}>
      <SignUp {...props} />
    </ApolloProvider>
  );
}
