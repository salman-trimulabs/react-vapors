import React from "react";
import { Typography } from "antd";

const { Title } = Typography;

export default function Dashboard() {
  return (
    <Typography>
      <Title style={{ textAlign: "left", maxWidth: "640px", margin: "auto" }}>
        User Profile
      </Title>
    </Typography>
  );
}
