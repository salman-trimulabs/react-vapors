import React, { useState } from "react";
import styled from "styled-components";
import { ThemeColors } from "../../theme";
import { Input, Button } from "antd";
import { ApolloAuthClient } from "../../api";
import { ApolloProvider } from "react-apollo";

const Wrapper = styled.div`
  background: ${ThemeColors.colors.default};
  colors: ${ThemeColors.colors.textColor};
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Form = styled.form`
  max-width: 600px;
  width: 100%;
  padding: 1rem;
  height: ${(props) => (props.isLoginView ? "250px" : "300px")};
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
`;

const LogIn = (props) => {
  const [isLoginView, setIsLoginView] = useState(true);
  const { setAuth } = props;

  return (
    <Wrapper>
      <Form theme={ThemeColors.colors.themeCode} isLoginView={isLoginView}>
        <h2 style={{ color: ThemeColors.colors.textColor }}>
          {isLoginView ? "Login" : "Sign Up"}
        </h2>

        {!isLoginView && (
          <Input
            size={"large"}
            placeholder="Your Full Name"
            onChange={(e) => console.log(e.target.value)}
          />
        )}
        <Input
          size={"large"}
          placeholder="Your email address"
          onChange={(e) => console.log(e.target.value)}
        />
        <Input.Password
          size={"large"}
          placeholder="Your password"
          onChange={(e) => console.log(e.target.value)}
        />
        <Button type="primary" size={"large"} onClick={() => setAuth(true)}>
          <div>{isLoginView ? "Login" : "Sign Up"}</div>
        </Button>
      </Form>
      <Button type="link" block onClick={() => setIsLoginView(!isLoginView)}>
        {isLoginView
          ? "Don't have an account? SignUp here."
          : "Already have an account? SignIn here."}
      </Button>
    </Wrapper>
  );
};

export { LogIn };

export default function LoginProvider(props) {
  return (
    <ApolloProvider client={ApolloAuthClient.client}>
      <LogIn {...props} />
    </ApolloProvider>
  );
}
