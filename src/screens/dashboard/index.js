import React from "react";
import { Typography } from "antd";
import styled from "styled-components";

const { Title } = Typography;

const ImageWrapper = styled.div`
  display: flex;
  max-width: 640px;
  margin: auto;
  img {
    width: 100%;
    max-width: 450px;
  }
`;

export default function Dashboard() {

  return (
    <Typography>
      <Title style={{ textAlign: "left", maxWidth: "640px", margin: "auto" }}>
        Hi
      </Title>
      <Title
        style={{ textAlign: "left", maxWidth: "640px", margin: "1rem auto" }}
        level={4}
      >
        Welcome to React Vapor
      </Title>
      <ImageWrapper>
        <div>Promo Image</div>
      </ImageWrapper>
    </Typography>
  );
}
