import React, { useState } from "react";

import { Icon, Layout, Menu, Breadcrumb } from "antd";
import { withRouter } from "react-router-dom";
// import {useQuery} from '@apollo/react-hooks';
// import {Queries} from './../api';
import Logout from "./Logout";
import "./layout.css";
import { ThemeColors } from "../theme";
const { Header, Content, Footer, Sider } = Layout;

function PrimaryLayout(props) {
  const [collapsed, setCollapsed] = useState(false);
  // const { loading: usersLoading, error: usersError, data: usersData} = useQuery(Queries.ALL_USERS_QUERY, {});
  // const { loading: orgLoading, error: orgError, data: orgData} = useQuery(Queries.ORGANIZATION, {});

  const paths = props.location.pathname.split("/").slice(1);
  const active = paths[0] === "" ? "dashboard" : paths[0];
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Header style={{ backgroundColor: ThemeColors.colors.default }}>
        <div className={"logo-container"}>Logo Here</div>
        <Menu
          style={{
            lineHeight: "64px",
          }}
          mode="horizontal"
          defaultSelectedKeys={["2"]}
        >
          {/*{*/}
          {/*orgData &&*/}
          {/*<Menu.Item key="1">{orgData.organization.title}</Menu.Item>*/}
          {/*}*/}
          {/*<Menu.Item key="2">nav 2</Menu.Item>*/}
          {/*<Menu.Item key="3">nav 3</Menu.Item>*/}
        </Menu>
      </Header>
      <Layout>
        <Sider
          collapsible
          collapsed={collapsed}
          onCollapse={() => setCollapsed(!collapsed)}
          theme={ThemeColors.colors.themeCode}
        >
          <Menu
            theme={ThemeColors.colors.themeCode}
            defaultSelectedKeys={[active]}
            mode="inline"
          >
            <Menu.Item key="dashboard" onClick={() => props.history.push("/")}>
              <Icon type="dashboard" />
              <span>Dashboard</span>
            </Menu.Item>
            <Menu.Item
              key="profile"
              onClick={() => props.history.push("/profile")}
            >
              <Icon type="user" />
              <span>Profile</span>
            </Menu.Item>
            <Menu.Item>
              <Logout setAuth={props.setAuth} />
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Content style={{ margin: "0 16px" }}>
            <Breadcrumb style={{ margin: "16px 0" }}>
              {paths.map((p) => (
                <Breadcrumb.Item key={p}>
                  {p.replace(/\b\w/, (v) => v.toUpperCase())}
                </Breadcrumb.Item>
              ))}
            </Breadcrumb>
            <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
              {props.children}
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}>React Vapor ©2020</Footer>
        </Layout>
      </Layout>
    </Layout>
  );
}

export default withRouter(PrimaryLayout);
