import React from "react";
import { Icon } from "antd";
import { ApolloAuthClient } from "../api";
import { ApolloProvider } from "react-apollo";

const LogoutProvider = (props) => {
  return (
    <ApolloProvider client={ApolloAuthClient.client}>
      <LogoutInner {...props} />
    </ApolloProvider>
  );
};

const LogoutInner = (props) => {
  return (
    <div key="logout" onClick={() => props.setAuth(false)}>
      <Icon type="logout" />
      <span>Logout</span>
    </div>
  );
};

export default LogoutProvider;
