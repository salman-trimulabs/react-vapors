const LightMode = {
  themeCode: "light",
  default: "#ffffff",
  textColor: "#00000",
  brand: "",
  primary: "#002140",
  secondary: "#002140"
};

const DarkMode = {
  themeCode: "dark",
  default: "#001529",
  textColor: "#ffffff",
  brand: "",
  primary: "#001529",
  secondary: "#002140"
};
const colorScheme =
  (process.env.REACT_APP_MODE ? process.env.REACT_APP_MODE : "2") === "2"
    ? LightMode
    : DarkMode;

const ThemeColors = {
  colors: {
    white: "#FFFFFF",
    lightWhite: "#F0F1F4",
    green: "#008000",
    lightGreen: "#f0fff0",
    red: " #ff0000",
    lightRed: "#ffe4e1",
    black: "#343434",
    grey: "#A0A9BA",
    lightGrey: "#E1E3EA",
    ...colorScheme,
  },
};

export default ThemeColors;
