import React, {useState} from 'react';
import 'antd/dist/antd.css';
import './App.css';
import PrimaryLayout from "./layouts";
import { ApolloProvider } from "react-apollo";
import { ApolloDataClient } from "./api";
import { Route, Switch, Router } from 'react-router';
import { createBrowserHistory } from 'history';
import Routes from './routes';
import LogIn from "./screens/logIn";

const history = createBrowserHistory();

function App() {
  const [auth, setAuth] = useState(false);
  return (
    <ApolloProvider client={ApolloDataClient.client}>
      <div className="App">
        {
          auth ?
          <Router history={history}>
            <PrimaryLayout
              setAuth={setAuth}
            >
              <Switch>
                {Routes.map(route => <Route key={`route-${route.name}`} {...route} />)}
              </Switch>
            </PrimaryLayout>
          </Router>
          :
          <LogIn
            setAuth={setAuth}
          />
        }
      </div>
    </ApolloProvider>
  );
}

export default App;
